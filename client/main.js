import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Players } from '../imports/api/players';

import '../imports/ui/body.js';

import './main.html';

Template.identify.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.playername = new ReactiveVar("");
});

Template.identify.helpers({
  playername() {
    return Template.instance().playername.get();
  },
});

Template.identify.events({
  'submit form'(event, instance) {
    event.preventDefault();
    // increment the counter when button is clicked

    const target = event.target;
    const playername = target.playername.value;

    Players.insert({
      name: playername,
      game: "123",
      createdAt: new Date()
    });

    // clear form
    target.playername.value = '';
  },
});
