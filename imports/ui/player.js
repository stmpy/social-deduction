import { Template } from 'meteor/templating';

import { Players } from '../api/players.js';

import './player.html'

Template.player.events({
  'click .remove'() {
    Players.remove(this._id);
  }
});
